﻿using UnityEngine;

public class Volume : MonoBehaviour
{
    [SerializeField] private AudioSource audio;
    private float volume = 0.1f;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        audio.volume = volume;
    }

    public void SetVolume(float _volume)
    {
        volume = _volume;
    }

    public float GetVolume()
    {
        return volume;
    }
}