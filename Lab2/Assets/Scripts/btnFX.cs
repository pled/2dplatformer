﻿using UnityEngine;

public class btnFX : MonoBehaviour
{
    [SerializeField] private AudioSource audio;
    [SerializeField] private AudioClip clickSound;

    public void ClickSound()
    {
        audio.PlayOneShot(clickSound);
    }
}